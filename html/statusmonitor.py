#!/usr/bin/env python3
# statusmonitor.py:
#                Wimborne Model Town
#      Minster Webserver Monitoring and Control Software
#
#  **********************************************************************
# This Flask Web App carries out the following functions:
#
#  1.  Provides a Status page to display CPU and Player Functions.
#  2.  Provides a Control page to allow Player Functions to be managed.

import config
import time

from flask import Flask, render_template, request, url_for, redirect
from flask_wtf import FlaskForm, form
from flask_httpauth import HTTPBasicAuth

import logging
from Tools import loggingtools

# List containing the System Status
# index           [0]                [1]                   [2]                   [3]               [4]
#                 [5]                        [6]                   [7]                 [8]            [9]
#             [10]               [11]                    [12] 
Status = ["Music CPU Load", "Music Memory Used", "Music CPU Temperature", "Bells CPU Load", "Bells Memory Used",
          "Bells CPU Temperature", "Music Pi System Time", "Bells Pi System Time", "System Mode", "Playlist Number",
          "Playlist", "Extended Hours Enabled", "Change Rings Title"]

socket = None                      # Define object name to be used in sockets code for the Webserver.

app = Flask(__name__)

def startup():
    global socket

    #Welcome message.
    logger.info("Minster Webserver Software Version "+config.VERSION+" ("+config.RELEASEDATE+")")
    logger.info("System startup sequence initiated.")

    print("Minster Webserver Software Version"+config.VERSION+" ("+config.RELEASEDATE+")")
    print("System startup sequence initiated.")

    print("System startup sequence initiated.")

    #Create all sockets.
    logger.info("Creating sockets...")
    sockets = {}

    # Set up connection to the Bells Pi
    site_settings = config.SITE_SETTINGS["MUSIC-C"]

    # We are a server to the Music Pi and we are hosting sockets.
    socket = socket_tools.Sockets("Socket", "WEBSERVER", "Music Pi Client Socket")
    socket.set_portnumber(site_settings["ServerPort"])
    socket.set_server_address(site_settings["IPAddress"])
    sockets[site_settings["SocketID"]] = socket

    socket.start_handler()

def init_logging():
    """
    Used as part of the logging initialisation process during startup.
    """

    logger = logging.getLogger('Minster Webserver Software') #pylint: disable=redefined-outer-name

    #Remove the console handler.
    logger.handlers = []

    #Set up the timed rotating file handler.
    rotator = loggingtools.CustomLoggingHandler(filename='logs/Minster-Webserver.log',
                                                when="midnight")

    logger.addHandler(rotator)

    #Set up the formatter.
    formatter = logging.Formatter(fmt='%(asctime)s - %(name)s - %(levelname)s: %(message)s',
                                  datefmt='%d/%m/%Y %H:%M:%S')

    rotator.setFormatter(formatter)

    #Default logging level of INFO.
    logger.setLevel(logging.INFO)
    rotator.setLevel(logging.INFO)

    return logger, rotator

def fetch_system_status():            # Retrieve Bells status
    global socket, Status

    data = ""
    status_received = False

    socket.write("Send System Status")    # Signal the Music Pi to send the system status info
    logger.info("Music Pi System Status Request Sent")
    print("Music Pi System Status Request Sent")

    while not status_received:
        if socket.has_data():                        # Check for message
            data = socket.read()
            status_received = True

            logger.info("Music Pi System Status Received")
            print("Music Pi System Status Received")

    Status = data

    socket.pop()



################################ Run Startup Code ########################################

logger, handler = init_logging()


from Tools import sockettools as socket_tools

startup()                               # Record Startup message and initialise sockets

print("Waiting 20 seconds for the Music Player to start")
time.sleep(20)

fetch_system_status()                   # Populate the Status List

######################################  Routes ##########################################

auth = HTTPBasicAuth()

users = {
    "pi": "minster2020!"
}

@auth.get_password
def get_pw(username):
    if username in users:
        return users.get(username)
    return None

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


@app.errorhandler(500)
def internal_server_error(e):
    return render_template('500.html'), 500


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        if request.form['submit_button'] == "Refresh Page":
            return redirect(url_for('index'))
        elif request.form['submit_button'] == "Switch to Control Functions Page":
            return redirect(url_for('control'))

    fetch_system_status()                   # Refresh the Status List

    return render_template('index.html', form=form, Status=Status)

@app.route('/control', methods=['GET', 'POST'])
@auth.login_required
def control():
    if request.method == 'POST':
        if request.form['submit_button'] == "Stop Music":
            socket.write("Stop Music")
        elif request.form['submit_button'] == "Start Music":
            socket.write("Start Music")
        elif request.form['submit_button'] == "Next Playlist":
            socket.write("Next Playlist")
        elif request.form['submit_button'] == "Stop Wedding Sequence":
            socket.write("Stop Wedding Sequence")
        elif request.form['submit_button'] == "Start Wedding Sequence":
            socket.write("Start Wedding Sequence")
        elif request.form['submit_button'] == "Stop Change Rings":
            socket.write("Stop Change Rings")
        elif request.form['submit_button'] == "Start Change Rings":
            socket.write("Start Change Rings")
        elif request.form['submit_button'] == "Enable Extended Hours":
            socket.write("Enable Extended Hours")
        elif request.form['submit_button'] == "Disable Extended Hours":
            socket.write("Disable Extended Hours")
        elif request.form['submit_button'] == "Switch to Status Page":
            return redirect(url_for('index'))
        else:
            pass

    time.sleep(5)                           # Allow time for commands to execute

    fetch_system_status()                   # Refresh the Status List

    return render_template('control.html', form=form, Status=Status)

if __name__ == '__main__':
    app.run(host='0.0.0.0')
