#! /usr/bin/env python3
# minstermusic.py:
#                Wimborne Model Town
#      Minster MP3 Player Software
#
#  **********************************************************************
# This program carries out the following functions:
#  1.  Monitors the time and triggers the following between Opening Hours
#      and Closing Hours of WMT.
#      a.  An MP3 Player function to allow the playing of background church
#          music in the Nave.
#      b.  A Wedding Sequence of:
#          i.  Wedding March.
#          ii.  Wedding vows.
#          iii. Widor's Toccata and Fugue.
#  2.  Generates the following time-based signals:
#      a.  Start music.
#      b.  Stop music.
#      c.  Start Wedding Sequence.
#  2.  Functions to respond to a Web-based User interface:
#      a.  Start music.
#      b.  Stop music.
#      c.  Next Track.
#      d.  Next Playlist
#      e.  Start Wedding Sequence.
#      f.  Stop Wedding Sequence.
#      g.  Start Change Rings.
#      h.  Stop Change Rings.
#      i.  Enable Extended Hours.
#      j.  Disable Extended Hours.
#
# It uses the apsscheduler library to determine the key trigger points for the
# time related functions.
#
# Communication between this Pi and the associated Minster Bells Pi is performed
# using the 'Sockets()' Class contained in the Python Module Tools/sockettools
# which was developed for the River System Project.
#
# A record of the status of the system will be held in a List and exchanged between
# this program and a Webserver also running on the same device.
#
# The Webserver will also interact with this program to allow user control of the
# Player and its associated functions.
#
# Copyright (c) 2021 Wimborne Model Town http://www.wimborne-modeltown.com/
#
#    This code is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Scheduler.py is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    with this software.  If not, see <http://www.gnu.org/licenses/>.
#
#  ***********************************************************************

from zoneinfo import ZoneInfo
from datetime import datetime, timedelta, timezone
import time
import os
import subprocess
import glob
from apscheduler.schedulers.background import BackgroundScheduler
import psutil
import config
import logging
import threading
import traceback
from Tools import loggingtools

################## Setup Variables - Use globals to make configuration easy.  ############################
# Vars used in multiple places.
enable_late = False                          # Late hours not OK (will be set / unset by switch operation).
hour = 10                                    # Hour in 24 hr format
opening_hours = False                        # When set, it's during Opening Hours (during late hours).
nave_vol = '90%'                             # Set the volume level fot ther chimes.

sched = BackgroundScheduler()                # Setup the scheduler

bells_socket = None                          # Define object name to be used in sockets code for Bells Pi.
webserver_socket = None                      # Define object name to be used in sockets code for the Webserver.

# MP3 Functions
# Locations for the different Playlists.
playlists = ['Music/Playlist1', 'Music/Playlist2', 'Music/Playlist3', 'Music/Playlist4', 'Music/Playlist5', 'Music/Playlist6']
playlist_index = 1                          # '0' means set to off by operator.  '1' to '6' means selected Playlist.  Default '2' to ensure cold start.
last_playlist_index = 1                     # Temporary store for use when stopping plyback
mp3_player = None                           # mpg123 object status.  When None, object not yet instantiated.

# Wedding Sequence control
ws_subdir = 'Music/Wedding_Sequence'# directory containing mp3 files for the Ceremony
wch_player = None                           # Nave mpg123 object status.  When None, not yet instantiated.

# List containing the System Status0x74ac96d0
# index           [0]                [1]                   [2]                   [3]               [4]
#                 [5]                        [6]                   [7]                 [8]            [9]
#             [10]               [11]                    [12] 
Status = ["Music CPU Load", "Music Memory Used", "Music CPU Temperature", "Bells CPU Load", "Bells Memory Used",
          "Bells CPU Temperature", "Music Pi System Time", "Bells Pi System Time", "System Mode", "Playlist Number",
          "Playlist", "Extended Hours Enabled", "Change Rings Title"]

################################################ Define Procedures  #######################################

def startup():
    global bells_socket, webserver_socket

    #Welcome message.
    logger.info("Minster Software Version "+config.VERSION+" ("+config.RELEASEDATE+")")
    logger.info("System startup sequence initiated.")

    print("Minster Software Version "+config.VERSION+" ("+config.RELEASEDATE+")")
    print("System Time: ", str(datetime.now()))
    print("System startup sequence initiated.")

    print("System startup sequence initiated.")

    #Create all sockets.
    logger.info("Creating sockets...")
    sockets = {}

    # Set up connection to the Bells Pi
    site_settings = config.SITE_SETTINGS["BELLS"]

    # We are a server to the Bells Pi and we are hosting sockets.
    bells_socket = socket_tools.Sockets("Socket", "MUSIC-S", "Bells Pi Sockets Client")
    bells_socket.set_portnumber(site_settings["ServerPort"])
    bells_socket.set_server_address(site_settings["IPAddress"])
    sockets[site_settings["SocketID"]] = bells_socket

    bells_socket.start_handler()

    # Accept connection to Webserver
    logger.info("Initialising connection to Webserver sockets server, please wait...")
    print("Initialising connection to Webserver sockets server, please wait...")
    webserver_socket = socket_tools.Sockets("Plug", "MUSIC-C", config.SITE_SETTINGS["MUSIC-C"]["SocketName"])
    webserver_socket.set_portnumber(config.SITE_SETTINGS["MUSIC-C"]["ServerPort"])
    webserver_socket.set_server_address(config.SITE_SETTINGS["MUSIC-C"]["ServerAddress"])

    webserver_socket.start_handler()

    logger.info("Waiting for peers to connect...")
    print("Waiting for peers to connect...")

    webserver_socket.pop()                     # Ensure that message buffer is empty

def monitor_performance():
    global Status
    # Called by apscheduler every hour or on demand
    cpu_percent = psutil.cpu_percent()
    Status[0] = str(cpu_percent)
    used_memory_mb = (psutil.virtual_memory().used // 1024 // 1024)
    Status[1] = str(used_memory_mb)
    cpu_temp = subprocess.check_output(['vcgencmd', 'measure_temp'])
    cpu_temp = float(cpu_temp[5:9])
    Status[2] = str(cpu_temp)

    timenow = str(datetime.now())
    Status[6] = timenow[0:16]

    print("\n\nCPU Usage (%): "+Status[0]
        + "\nMemory Used (MB): "+Status[1]
        + "\nCPU Temperature (Deg C): "+Status[2]
        + "\n\n")

    logger.info("\n\nCPU Usage (%): "+Status[0]
        + "\nMemory Used (MB): "+Status[1]
        + "\nCPU Temperature (Deg C): "+Status[2]
        + "\n\n")

def checkpingstate(host):
    response = os.system("ping -c 1 " + host)
    if (response == 0):
        pingstatus = True
    else:
        pingstatus = host + " is Not reachable"
        pingstatus = False
    return pingstatus

def restart_to_restore_wifi():
    global opening_hours

    if not opening_hours:
        logger.info("WiFi Down")
        reset()

    else:
        logger.info("WiFi Down, but still Opening Hours.  Defer reboot.")

def checkwifihosts():
    host1 = "192.168.0.30"      # VPN Server
    host2 = "192.168.0.253"     # Garden Seat Shelter Antenna
    host3 = "192.168.0.254"     # Gazebo Antenna

    host_status = ""

    host1_status = checkpingstate(host1)
    host2_status = checkpingstate(host2)
    host3_status = checkpingstate(host3)

    if not host1_status:
        host_status = host1 + " Down"

    else:
        host_status = host1 + " Up"

#    print (host_status)
    logger.info(host_status)

    if not host2_status:
        host_status = host2 + " Down"

    else:
        host_status = host2 + " Up"

#    print (host_status)
    logger.info(host_status)

    if not checkpingstate(host3):
        host_status = host3 + " Down"

    else:
        host_status = host3 + " Up"

#    print (host_status)
    logger.info(host_status)

    if (host1_status or host2_status or host3_status):
        return True

    else:
        return False

def checkwifi():
    if not checkwifihosts():
        logger.info("WiFi Down")
        restart_to_restore_wifi()
    else:
        pass

def check_silent_hours():
    global opening_hours, hour, Status

    now = datetime.now()
    hour = now.hour                         # Get the actual hour in 24 hr format

    print("In check_silent_hours(). Hour = " + str(hour))

    if hour >= 17:                          # Check if time after normal daytime opening
        if enable_late is False:            # If it is, make sure that Late Opening has not been set
            print("Silent Hours")
            opening_hours = False
            Status[8] = "Not Opening Hours"
        else:                               # Late Opening set, so ringing OK until 10.
            if hour <= 22:
                print("Late hours")
                opening_hours = True
                Status[8] = "Late Opening Hours"
    else:                                   # Daytime Opening hours
        if hour >= 10:
            print("Opening Hours")
            opening_hours = True
            Status[8] = "Opening Hours"
        else:
            print("Silent Hours")
            opening_hours = False
            Status[8] = "Not Opening Hours"

def hours():
    global Status

    check_silent_hours()
    if opening_hours is False:
        pass                                # Do nothing
    else:                                   # Play the sequence using the delays defined above
        print(datetime.now())

def set_volume():
    subprocess.call(['amixer', '-D', 'pulse', 'set', 'Master', nave_vol])

def extended_hours_trigger():
    global enable_late

    print("Extended Hours Enabled.\n")
    enable_late = True
    print (enable_late)
    Status[11] = "Enabled"

def extended_hours_disable():
    global enable_late

    print("Extended Hours Disabled\n")
    enable_late = False
    print(enable_late)
    Status[11] = "Disabled"

def stop_playback():

    mp3_player_stop()
    wedding_sequence_stop()

def shutdown():                                         # If outside Opening Hours stop all the players
    if not opening_hours:
        print('Shut down everything')

        stop_playback()

def scheduledws():                                      # Run the Scheduled Wedding Sequence
    if opening_hours:
        if hour == 11:
            print('11 oclock')
            print('Start the Scheduled Wedding Sequence')
            logger.info("Start the Scheduled Wedding Sequence - Morning")
            wedding_sequence_start()
        elif hour == 14:
            print('2 oclock')
            print('Start the Scheduled Wedding Sequence - Afternoon')
            logger.info("Start the Scheduled Wedding Sequence - Afternoon")
            wedding_sequence_start()
        else:
            print('Not time for Wedding Sequence')

def scheduledmp3():                                      # Run the Scheduled MP3 Player (10:04 am)
    global playlist_index, last_playlist_index
#    logger.info("6 Minute apscheduler Task Triggered")

    playlist_index = last_playlist_index                 # stop_playback sets it ti zero.

    if opening_hours:
        if hour == 10:
            print('Six Minutes past 10 oclock in the morning')
            print('Start the Scheduled MP3 Player using the same Playlist as when stopped')
            logger.info("10 am Startup Triggered")
            if playlist_index > 0:
                mp3_player_start()                      # (mp3_player_start() increments the index.)
            print('MP3 Player wasn''t running when system was disabled')
        else:
            print('Not hour for scheduled MP3 Player')

def mp3_player_start():                          #  Setup and run the MP3 Player
    global playlists, playlist_index, last_playlist_index, mp3_player, Status

    stop_playback()

    playlist_index  = last_playlist_index   # stop_playback sets it ti zero.

    logger.info("playlist_index = " + str(playlist_index))

    while playlist_index != 0:
        print("Playlist Number = " + str(playlist_index))
        logger.info("Playlist No = " + str(playlist_index))
        print("Number of Playlists = " + str(len(playlists)))

        mp3_subdir = playlists[playlist_index - 1]      # directory containing mp3 files for the current Playlist

        Status[9] = str(playlist_index)                 # Current Playlist Number
        Status[10] = os.listdir(mp3_subdir)             # Playlist

        print(Status[10])

        playlist = sorted(glob.glob(mp3_subdir + '/*.[Mm][Pp]3'))   # Create a list of files to be played in the current directory

        mpg_list = ['mpg123']

        args = mpg_list + playlist

        if mp3_player == None:
            mp3_player = subprocess.Popen(args)

        print("Music Playing")

        mp3_wait = mp3_player.wait()

        if not mp3_wait:
            print('MP3 Player: mpg123 failed:%#x\n' % mp3_wait)

        if playlist_index == 0:
            print("Break from Playlist Loop")
            break

        print("")
        print("mp3_player Value = ")
        print(mp3_player)
        print("")

        next_playlist()

def mp3_player_stop():                           #  Setup and run the MP3 Player
    global mp3_player, playlist_index, last_playlist_index, Status

    last_playlist_index = playlist_index

    playlist_index = 0                          # Not Playing Flag.

    if mp3_player is None:
        return

    mp3_player.terminate()
    mp3_wait = mp3_player.wait()
    if not mp3_wait:
        print('MP3 Player: mpg123 failed:%#x\n' % mp3_wait)
    mp3_player = None

    print("Music Stopped")
    logger.info("Music Stopped")

    Status[10] = "Not Playing"

def next_playlist():
    global playlists, playlist_index

    playlist_index += 1                                  # Select the next Playlist Number

    if playlist_index > len(playlists):
        playlist_index = 1

    Status[9] = str(playlist_index)

    mp3_player_start()

def wedding_sequence_start():                    # Play a wedding ceremony and change rings
    global wch_player, ws_subdir, bells_socket, Status

    stop_playback()

    logger.info("Wedding Sequence Triggered")

    Status[9] = "Wedding Sequence"                 # Wedding Playlist
    Status[10] = os.listdir(ws_subdir)             # Playlist

    print(Status[10])

    ws_playlist = sorted(glob.glob(ws_subdir + '/*.[Mm][Pp]3'))   # Create a list of files to be played in the current directory

    ws_mpg_list = ['mpg123']

    ws_args = ws_mpg_list + ws_playlist

    wch_player = subprocess.Popen(ws_args)

    print('Playing Wedding Sequence to the Nave')

    print("Wait 7 Minutes After Start of Music Before Starting Changes")

    time.sleep(60*6)                         # Wait 6 minutes before starting the Changes

    bells_socket.write("Wedding Changes Start")    # Signal the Bells Pi to start the Changes

    print("Wedding Changes Start Sent to Bells Pi")    # Signal the Bells Pi to start the Changes

    wch_wait = wch_player.wait()

    if not wch_wait:
        print('Wedding Sequence in Nave: mpg123 failed:%#x\n' % wch_wait)
    wch_player = None

    Status[10] = "Wedding Music Finished"

    print("Wait 4 More Minutes Before Restarting Music")

    time.sleep(60*4)                         # Wait 5 more minutes.

    logger.info("Wedding Sequence Finished")

    if opening_hours is True:
        restartmp3()                             # Restart MP3 Player.

def wedding_sequence_stop():                           #  Stop the Wedding Sequence
    global wch_player, Status, bells_socket

    if wch_player == None:
        return

    if wch_player:
        wch_player.terminate()
        wch_wait = wch_player.wait()
        if not wch_wait:
            print('Wedding Sequence in Nave: mpg123 failed:%#x\n' % wch_wait)
        wch_player = None

        print('Wedding Sequence Terminated')
        logger.info("Wedding Sequence Terminated")

    bells_socket.write("Stop Wedding Rings")    # Signal the Bells Pi to stop the Bells

    Status[10] = "Wedding Sequence Terminated"

    restartmp3()                             # Restart MP3 Player.

def restartmp3():                           # Restart the MP3 Player if the Wedding Sequence is completed.
    global playlist_index, last_playlist_index

    playlist_index  = last_playlist_index   # Restore the Plylist Number to what it was last time the Player was stopped.

    logger.info("Music Restarted")

    mp3_player_start()                      # (mp3_player_start() increments the index.)

def change_rings_stop():
    global bells_socket

    bells_socket.write("Stop Change Rings")    # Signal the Bells Pi to stop the Bells
    print("Stop Change Rings Sent to Bells Pi")
    logger.info("Change Rings Stopped")

def change_rings_start():
    global bells_socket

    bells_socket.write("Start Change Rings")    # Signal the Bells Pi to start the Bells
    print("Start Change Rings Sent to Bells Pi")
    logger.info("Change Rings Triggered")

def reset():                   # Reboot the Pi to maintain WiFi connection and reset everything
    print('Rebooting Pi in 30 seconds..')
    logger.info("Rebooting Pi in 30 seconds..")

    time.sleep(30)

    subprocess.call('reboot')

def resetnormalopening():                   # Reset Opening Hours to normal after the site has closed for the night.
    global enable_late

    enable_late = False
    print('Opening Hours reset to normal')

def fetch_minsterbells_status():            # Retrieve Bells status
    global bells_socket, Status

    data = ""
    status_received = False

    bells_socket.write("Send Bells Status")    # Signal the Bells Pi to send its status info
    print("Bells Pi Status Request Sent")

    while not status_received:
        if bells_socket.has_data():                        # Check for message
            data = bells_socket.read()
            status_received = True

            print("Bells Pi Status Received")
            logger.info("Bells Pi Status Received")

    Status[3:6] = data[3:6]
    Status[7] = data[7]
    Status[12] = data[12]

    print("Status Values written to Status[]")

    bells_socket.pop()

def check_webserver_commands():               # Act upon commands from Minster Music Pi
    global webserver_socket, bells_socket, Status

    sched.pause_job(job_id='check_webserver_commands') # Suspend intil current job is complete

    data = ""

    if webserver_socket.has_data():                     # Check for message
        data = webserver_socket.read()

        if "Status" in data:                      # Status request from Music Pi Received
            print("Webserver Status Request Received")
            logger.info("Webserver Status Request Received")
            bells_socket.write("Send Bells Status")    # Signal the Bells Pi to send its status info
            fetch_minsterbells_status()           # Read the response
            monitor_performance()                 # Get the current values
            webserver_socket.write(Status)        # Send the System status to the Webserver
            print("Status sent to Webserver")

        elif "Stop Music" in data:          # Signal from Music Pi Received
            print("Stop Music from Web Controller")
            logger.info("Stop Music from Web Controller")
            threading.Thread(target=mp3_player_stop).start()

        elif "Start Music" in data:          # Signal from Music Pi Received
            print("Start Music from Web Controller")
            logger.info("Start Music from Web Controller")
            threading.Thread(target=restartmp3).start()

        elif "Next Playlist" in data:          # Signal from Music Pi Received
            print("Next Playlist from Web Controller")
            logger.info("Next Playlist from Web Controller")
            threading.Thread(target=next_playlist).start()

        elif "Stop Wedding Sequence" in data:          # Signal from Music Pi Received
            print("Stop Wedding Sequence from Web Controller")
            logger.info("Stop Wedding Sequence from Web Controller")
            threading.Thread(target=wedding_sequence_stop).start()

        elif "Start Wedding Sequence" in data:          # Signal from Music Pi Received
            print("Start Wedding Sequence from Web Controller")
            logger.info("Start Wedding Sequence from Web Controller")
            threading.Thread(target=wedding_sequence_start).start()

        elif "Enable Extended Hours" in data:          # Signal from Music Pi Received
            print("Enable Extended Hours from Web Controller")
            logger.info("Enable Extended Hours from Web Controller")
            threading.Thread(target=extended_hours_trigger).start()

        elif "Disable Extended Hours" in data:          # Signal from Music Pi Received
            print("Disable Extended Hours from Web Controller")
            logger.info("Disable Extended Hours from Web Controller")
            threading.Thread(target=extended_hours_disable).start()
            extended_hours_disable()

        elif "Stop Change Rings" in data:          # Signal from Music Pi Received
            print("Stop Change Rings from Web Controller")
            logger.info("Stop Change Rings from Web Controller")
            threading.Thread(target=change_rings_stop).start()

        elif "Start Change Rings" in data:          # Signal from Music Pi Received
            print("Start Change_Rings from Web Controller")
            logger.info("Start Change Rings from Web Controller")
            threading.Thread(target=change_rings_start).start()

        else:
            print("Error: Unexpected Message Received from Webserver: ")
            print(data)
            logger.error("Error: Unexpected Message Received from Webserver: ")
            logger.error(data)

    sched.resume_job(job_id='check_webserver_commands') # Restart checking for commands

    webserver_socket.pop()

def funcs_schedule():                      # Schedule functions to be run and start the scheduler
#    sched.add_job(reset, 'cron', hour='09')
    sched.add_job(hours, 'cron', hour='10')
    sched.add_job(hours, 'cron', hour='11')
    sched.add_job(hours, 'cron', hour='12')
    sched.add_job(hours, 'cron', hour='13')
    sched.add_job(hours, 'cron', hour='14')
    sched.add_job(hours, 'cron', hour='15')
    sched.add_job(hours, 'cron', hour='16')
    sched.add_job(hours, 'cron', hour='17')
    sched.add_job(hours, 'cron', hour='18')
    sched.add_job(hours, 'cron', hour='19')
    sched.add_job(hours, 'cron', hour='20')
    sched.add_job(hours, 'cron', hour='21')
    sched.add_job(hours, 'cron', hour='22')
    sched.add_job(resetnormalopening, 'cron', hour='23')
    sched.add_job(checkwifi, 'cron', minute='51')
    sched.add_job(shutdown, 'cron', minute='1')
    sched.add_job(scheduledws, 'cron', minute='3')
    sched.add_job(scheduledmp3, 'cron', minute='6')
    sched.add_job(check_webserver_commands, 'interval', seconds=1, id='check_webserver_commands')

    sched.start()

def init_logging():
    """
    Used as part of the logging initialisation process during startup.
    """

    logger = logging.getLogger('Minster Software') #pylint: disable=redefined-outer-name

    #Remove the console handler.
    logger.handlers = []

    #Set up the timed rotating file handler.
    rotator = loggingtools.CustomLoggingHandler(filename='./logs/Minster-Music.log',
                                                when="midnight")

    logger.addHandler(rotator)

    #Set up the formatter.
    formatter = logging.Formatter(fmt='%(asctime)s - %(name)s - %(levelname)s: %(message)s',
                                  datefmt='%d/%m/%Y %H:%M:%S')

    rotator.setFormatter(formatter)

    #Default logging level of INFO.
    logger.setLevel(logging.INFO)
    rotator.setLevel(logging.INFO)

    return logger, rotator


################################################ Main Program Begins Here  ###########################################

if __name__ == "__main__":
    #---------- SET UP THE LOGGER ----------
    logger, handler = init_logging()

    from Tools import sockettools as socket_tools

try:
    logger.info("playlist_index = " + str(playlist_index))
    logger.info("last_playlist_index = " + str(last_playlist_index))
    print("Startup at ", str(datetime.now()))

    startup()                               # Record Startup message and initialise sockets

    # Initialise stats which haven't been set yet.
    Status[3] = "Bells Pi Not Interrogated Yet"
    Status[4] = "Bells Pi Not Interrogated Yet"
    Status[5] = "Bells Pi Not Interrogated Yet"
    Status[7] = "Bells Pi Not Interrogated Yet"
    Status[8] = "Opening Hours not Determined Yet"
    Status[9] = "Music Player Not Running"
    Status[10] = "Music Player Not Running"
    Status[11] = "Extended Hours Status not Set Yet"
    Status[12] = "Bells Pi Not Interrogated Yet"

    set_volume()

    check_silent_hours()

    funcs_schedule()

    if opening_hours:                       # Program has begun after 10 am, so start the music.
        mp3_player_start()

    while 1:                                # Otherwise, idle until Opening Hours is true and an event occurs.
        print("In long sleep")
        time.sleep(2**31-1)

#Catch any unexpected errors and log them so we know what happened.
except Exception:
    logger.critical("Unexpected error \n\n"+str(traceback.format_exc())
                    +"\n\nwhile running. Exiting...")

    print("Unexpected error \n\n"+str(traceback.format_exc())+"\n\nwhile running. Exiting...")

    bells_socket.wait_for_handler_to_exit()
    bells_socket.reset()
