#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Configuration for the Minster Bells and Music Software
# Copyright (C) 2020-2021 Wimborne Model Town
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3 or,
# at your option, any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
This is the configuration for both pis in the Minster Music and Bells System.
These dictionaries (key-value base data) provide the configuration for
each device in a centralised, easy-to-change file.

This takes the form of a dictionary object named "SITE_SETTINGS".
This object has detailed configuration for each site, namely the ID,
sockets to host (if any), (local) probes to monitor, and devices to
control.

In the dictionary, devices are identified according to their location
and type the following key, eg:

* Minster Music Player: MUSIC-S   - MP3 Player and Sockets server.

* Minster Bells Player: BELLS     - Bells Player and Sockets client.

* Minster Music Player: MUSIC-C   - MP3 Player and Sockets client.

* Minster Webserver:    WEBSERVER - Monitoring and Control Webserver
                                    and Sockets server.

Notes:

1.  The code to actually make decisions and decide what to do with
the devices to control and monitor is not here; it's in the individual
programs in each Pi.

2.  These configuration details are mainly used in sockettools and to
identify useful information such as the version and modification date.

3.  The Music Pi includes a Webserver connected to the WMT Network via
wlan0.  It therefore been allocated a Static IP Address on the Ethernet
interface (eth0) that will not conflict with any of the other devices
on that network.

4.  The Music Pi is connected to the Bells Pi with a USB Cable, which
has been configured as a network device using an overlay.  This gives
the Bells Pi a network ID (usb0) and that causes the Music Pi to
autmatically configure itself with the same network ID.  This obviates
the need for a ESB / Ethernet Adaptor.

There are no classes or functions defined in this file.

.. module:: config.py
    :platform: Linux
    :synopsis: The configuration for the control software.

.. moduleauthor:: Hamish McIntyre-Bhatty <hamishmb@live.co.uk>
.. and Terry Coles <wmt@hadrian-way.co.uk>


"""

#Define global variables.
VERSION = "0.2.4"
RELEASEDATE = "15/02/2021"

#System ID of this Pi.
SYSTEM_ID = None

#CPU LOAD and MEMORY USAGE (MB).
CPU = None
MEM = None

#List of sockets objects.
SOCKETSLIST = []

#Used to signal software shutdown to all the threads.
EXITING = False

#Signals whether we are in debug mode.
DEBUG = False

#Used to signal pending shutdown, reboot, and update.
SHUTDOWN = False
SHUTDOWNALL = False
REBOOT = False
REBOOTALL = False
UPDATE = False

SITE_SETTINGS = {

    #Settings for the MUSIC Player (sockets server for connection to BELLS).
    "MUSIC-S":
        {
            "Type": "Player",
            "ID": "MUSIC-S",
            "Name": "Music Pi Sockets Server",
            "Default Interval": 1,
            "IPAddress": "192.168.10.1",
            "HostingSockets": True,
        },

    #Settings for the MUSIC Player (sockets client for connection to WEBSERVER).
    "MUSIC-C":
        {

            "Type": "Player",
            "ID": "MUSIC-C",
            "Name": "Music Pi Sockets Client",
            "Default Interval": 1,
            "IPAddress": "127.0.0.1",

            "ServerAddress": "127.0.0.1",
            "ServerPort": 31001,
            "ServerName": "WEBSERVER",
            "SocketName": "Webserver Socket",
            "SocketID": "SOCK2"
        },

    #Settings for the BELLS Player (client pi for connection to MUSIC-S).
    "BELLS":
        {
            "Type": "Player",
            "ID": "BELLS",
            "Name": "Bells Pi Sockets Client",
            "Default Interval": 1,
            "IPAddress": "192.168.10.2",

            "ServerAddress": "192.168.10.1",
            "ServerPort": 31002,
            "ServerName": "MUSIC-S",
            "SocketName": "Music Pi Server Socket",
            "SocketID": "SOCK1"
        },

    #Settings for the Webserver (sockets server for connection to MUSIC-C).
    "WEBSERVER":
        {
            "Type": "Webserver",
            "ID": "WEBSERVER",
            "Name": "Webserver Sockets Server",
            "Default Interval": 1,
            "IPAddress": "127.0.0.1",
            "HostingSockets": True,
        },

}
