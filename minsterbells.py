#!/usr/bin/env python3
# minsterbells.py V0.1:
#                Wimborne Model Town
#      Minster Bells Timing amd Player Software
#
#  **********************************************************************
# This program carries out the following functions:
#  1.  Triggers the playing of a recording of the Minster chimes at the hours.
#  2.  Triggers the Quarter Jack hammer at the quarters.
#  3.  Triggers the playing of a recording of the Quarter Jack bells at the quarters from the Tower.
#  4.  Provides an MP3 Player to play change rings when required.
#  5.  Plays Change Rings at the end of the Wedding Sequence started by the Minster MP3 Player.
#  7.  User interface in the form of toggle switches.
#  8.  Responds to commands from the Web-based User interface in the Minster MP3 Player:
#      a.  Start Change Rings.
#      b.  Stop Change Rings.
#
# It uses the apsscheduler library to determine the key trigger points for the time related functions.
#
# Copyright (c) 2020 Wimborne Model Town http://www.wimborne-modeltown.com/
#
#    This code is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Scheduler.py is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    with this software.  If not, see <http://www.gnu.org/licenses/>.
#
#  ***********************************************************************

from zoneinfo import ZoneInfo
from datetime import datetime, timedelta, timezone
import time
import os
import glob
import subprocess
from apscheduler.schedulers.background import BackgroundScheduler
import RPi.GPIO as GPIO
import psutil
import config
import logging
import threading
import traceback
from Tools import loggingtools


################################### Setup GPIO  ######################################

GPIO.setmode(GPIO.BCM)

GPIO.setup(17, GPIO.OUT)                    # Left Solenoid (Quarter Jack arm) Actuator
GPIO.setup(27, GPIO.OUT)                    # Right Solenoid (Quarter Jack arm) Actuator
GPIO.output(17, GPIO.LOW)                   # Initialise Left Solenoid
GPIO.output(27, GPIO.LOW)                   # Initialise Right Solenoid


################## Setup Variables - Use globals to make configuration easy.  ############################

# Vars used in multiple places.
enable_late = False                          # Late hours not OK (will be set / unset by switch operation).
hour = 10                                    # Hour in 24 hr format
opening_hours = False                        # When set, it's during Opening Hours (during late hours).
tower_vol = '80%'                            # Set the volume level for the chimes.

# Chimes and Quarterjack functions.    All times in seconds.
quarters_delay = 120                        # Delay after real Minster bells before quarters
hours_chime_delay = 4                       # Delay after Quarterjack strikes the last pair of the hour
quarter = 15                                # Which quarter?
chimes = 10                                 # Number of chimes to ring
quarter_rings = 1                           # Number of Quaterjack pairs to ring
chms_subdir = 'Rings/Chimes'                # Directory containing mp3 files for the chimes

sched = BackgroundScheduler()               # Setup the scheduler

socket = None                               # Define object name to be used in sockets code.

# Change Rings control
chr_subdir = 'Rings/Change_Rings'           # directory containing the mp3 file for the Change Rings.
c_player = None                             # mpg123 object status.  When None, object not instantiated..

# Wedding Sequence control
wb_subdir = 'Rings/Wedding_Bells'           # directory containing mp3 file for the wedding changes
wtr_player = None                           # Tower mpg123 object status.

# List containing the System Status
# index           [0]                [1]                   [2]                   [3]               [4]
#                 [5]                        [6]                   [7]                 [8]            [9]
#             [10]               [11]                    [12] 
Status = ["Music CPU Load", "Music Memory Used", "Music CPU Temperature", "Bells CPU Load", "Bells Memory Used",
          "Bells CPU Temperature", "Music Pi System Time", "Bells Pi System Time", "System Mode", "Playlist Number",
          "Playlist", "Extended Hours Enabled", "Change Rings Title"]

################################################ Define Procedures  #######################################

def startup():
    global socket

    #Welcome message.
    logger.info("Minster Software Version "+config.VERSION+" ("+config.RELEASEDATE+")")
    logger.info("System startup sequence initiated.")

    print("Minster Software Version "+config.VERSION+" ("+config.RELEASEDATE+")")
    print("System Time: ", str(datetime.now()))
    print("System startup sequence initiated.")

    print("System startup sequence initiated.")

    #Create all sockets.
    logger.info("Creating sockets...")

    #Connect to the server on Minster Music.
    logger.info("Initialising connection to Minster Music server, please wait...")
    print("Initialising connection to Minster Music server, please wait...")
    socket = socket_tools.Sockets("Plug", "BELLS", config.SITE_SETTINGS["BELLS"]["SocketName"])
    socket.set_portnumber(config.SITE_SETTINGS["BELLS"]["ServerPort"])
    socket.set_server_address(config.SITE_SETTINGS["BELLS"]["ServerAddress"])

    socket.start_handler()

    logger.info("Waiting for peers to connect...")
    print("Waiting for peers to connect...")

    socket.pop()                             # Ensure that message buffer is empty

def monitor_performance():
    global Status
    # Called by apscheduler every 30 minutes or on demand
    cpu_percent = psutil.cpu_percent()
    Status[3] = str(cpu_percent)
    used_memory_mb = (psutil.virtual_memory().used // 1024 // 1024)
    Status[4] = str(used_memory_mb)
    cpu_temp = subprocess.check_output(['vcgencmd', 'measure_temp'])
    cpu_temp = float(cpu_temp[5:9])
    Status[5] = str(cpu_temp)

    timenow = str(datetime.now())
    Status[7] = timenow[0:16]

    print("\n\nCPU Usage (%): "+Status[3]
        + "\nMemory Used (MB): "+Status[4]
        + "\nCPU Temperature (Deg C): "+Status[5]
        + "\n\n")

    logger.info("\n\nCPU Usage (%): "+Status[3]
        + "\nMemory Used (MB): "+Status[4]
        + "\nCPU Temperature (Deg C): "+Status[5]
        + "\n\n")


def calculate_hours():
    global chimes

    print("In calculate_hours(). Hour = " + str(hour))
    if hour > 12:                           # Calculate the number of chimes to sound.
        chimes = hour - 12
    else:
        chimes = hour

    print(chimes)

def check_silent_hours():
    global enable_late, opening_hours, hour

    now = datetime.now()

    hour = now.hour                         # Get the actual hour in 24 hr format

    print("In check_silent_hours(). Hour = " + str(hour))

    if hour >= 17:                          # Check if time after normal daytime opening
        if enable_late is False:            # If it is, make sure that Late Opening has not been set
            print("Silent Hours")
            opening_hours = False
        else:                               # Late Opening set, so ringing OK until 10.
            if hour <= 22:
                print("Late hours")
                opening_hours = True
    else:                                   # Daytime Opening hours
        if hour >= 10:
            print("Opening Hours")
            opening_hours = True
        else:
            print("Silent Hours")
            opening_hours = False

def hours():
    global chimes, opening_hours

    check_silent_hours()
    if opening_hours is False:
        pass                                # Do nothing
    else:                                   # Play the sequence using the delays defined above
        calculate_hours()
        print(datetime.now())
        print(str(chimes) + " O'Clock")
        fc_file = 'One_Chime.mp3'           # File for the 1st and subsequent chimes
        lc_file = 'End_Chime.mp3'           # File for the last chime
        fchimes_path = os.path.join(chms_subdir, fc_file)
        lchime_path = os.path.join(chms_subdir, lc_file)
        quarters()
        time.sleep(hours_chime_delay)
        while chimes > 1:
            print(chimes)
            subprocess.call(['mpg123', fchimes_path])
            chimes -= 1
        subprocess.call(['mpg123', lchime_path])
        print("Last Chime")

def calculate_quarters():
    global quarter_rings, quarter

    now = datetime.now()
    quarter = now.minute                      # Get the actual hour in 24 hr format

    if quarter == 15:                         # Calculate the number of quarterjack pairs to sound.
        quarter_rings = 1
    elif quarter == 30:
        quarter_rings = 2
    elif quarter == 45:
        quarter_rings = 3
    else:
        quarter_rings = 4

def strike_hammers():
    GPIO.output(17, GPIO.HIGH)       # Strike with Left Solenoid
    time.sleep(0.05)
    GPIO.output(17, GPIO.LOW)        # Release Left Solenoid
    time.sleep(0.5)
    GPIO.output(27, GPIO.HIGH)       # Strike with Right Solenoid
    time.sleep(0.25)
    GPIO.output(27, GPIO.LOW)        # Release Right Solenoid
    time.sleep(0.2)

def quarters():
    global quarter_rings, opening_hours

    check_silent_hours()
    calculate_quarters()
    if opening_hours is False:
        pass                                 # Do nothing
    else:                                    # Play the sequence using the delays defined above
        print(datetime.now())
        print("Quarter No " + str(quarter_rings))
        fq_file = 'Wimborne_One_Quarter.mp3'           # File for one Quarterjack pair of chimes
        lq_file = 'Wimborne_End_Quarter.mp3'           # File for the last Quarterjack pair of chimes
        fq_path = os.path.join(chms_subdir, fq_file)
        lq_path = os.path.join(chms_subdir, lq_file)
        time.sleep(quarters_delay)
        while quarter_rings > 1:
            print("In While Loop" + str(quarter_rings))
            print(quarter_rings)
            subprocess.Popen(['mpg123', fq_path])
            strike_hammers()
            time.sleep(1)

            quarter_rings -= 1

        subprocess.Popen(['mpg123', lq_path])
        strike_hammers()
        print("Last Quarter Pair")

def set_volume():
    subprocess.call(['amixer', '-D', 'pulse', 'set', 'Master', tower_vol])

def extended_hours_trigger():
    global enable_late

    print("Extended Hours Enabled.\n")
    logger.info("Extended Hours Enabled.")
    enable_late = True
    print (enable_late)
    Status[11] = "Enabled"

def extended_hours_disable():
    global enable_late

    print("Extended Hours Disabled\n")
    logger.info("Extended Hours Disabled")
    enable_late = False
    print (enable_late)
    Status[11] = "Disabled"

def stop_playback():
    change_rings_stop()
    wedding_rings_stop()

def shutdown():                                         # If outside Opening Hours stop all the players
    global opening_hours
    
    if not opening_hours:
        print("Shut down everything")
        logger.info("Shut down everything")
        stop_playback()

def change_rings_start():                         # Play a file of change rings
    global c_player

    stop_playback()

    print("Change rings stopped")
    logger.info("Change rings stopped")

    c_file = 'Wimborne_Minster_StedmanCinques_16-10-2016.mp3' # Change rings.
    c_path = os.path.join(chr_subdir, c_file)
    blackhole = open(os.devnull, 'w')

    if c_player is None:
        c_player = subprocess.Popen(['mpg123', c_path], stdin=subprocess.PIPE, stdout=blackhole, stderr=blackhole)

    Status[12] = c_file

    print("Playing Changes - ", c_file, "to the Tower")
    logger.info("Playing Changes - ", c_file, "to the Tower")

def change_rings_stop():                         # Play a file of change rings
    global c_player


    if c_player is None:
        return

    c_player.terminate()
    c_wait = c_player.wait()
    if not c_wait:
        print ("Change Rings: mpg123 failed:%#x\n" % c_wait)
    c_player = None

    Status[12] = "Bells not Ringing"

def wedding_rings_start():                    # Play the change rings at the end of the wedding ceremony
    global wtr_player, wb_subdir, socket, Status

    print("Wedding Rings Starting")
    logger.info("Wedding Rings Starting")

    wb_playlist = sorted(glob.glob(wb_subdir + '/*.[Mm][Pp]3'))   # Create a list of files to be played in the current directory

    Status[12] = os.listdir(wb_subdir)             # Playlist

    wb_mpg_list = ['mpg123']

    wb_args = wb_mpg_list + wb_playlist

    print("Playing Wedding bells to the Tower")
    logger.info("Playing Wedding bells to the Tower")

    if wtr_player is None:
        wtr_player = subprocess.Popen(wb_args)

    print("Wedding Bells Finished")
    logger.info("Wedding Bells Finished")

    Status[12] = "Wedding Bells Finished"

def wedding_rings_stop():                           #  Stop the Wedding Sequence
    global wtr_player

    if wtr_player:
        wtr_player.terminate()
        wtr_wait = wtr_player.wait()
        if not wtr_wait:
            print ("Wedding Sequence in Tower: mpg123 failed:%#x\n" % wtr_wait)
        wtr_player = None

    Status[12] = "Bells not Ringing"

    print("Wedding Sequence Terminated")
    logger.info("Wedding Sequence Terminated")

def resetnormalopening():                   # Reset Opening Hours to normal after the site has closed for the night.
    global enable_late

    enable_late = False
    print("Opening Hours reset to normal")
    logger.info("Opening Hours reset to normal")
    Status[11] = "Disabled"

def check_minstermusic_commands():            # Act upon commands from Minster Music Pi
    global socket, Status

    sched.pause_job(job_id='check_minstermusic_commands') # Suspend intil current job is complete

    data = ""

    if socket.has_data():                     # Check for message
        data = socket.read()
        socket.pop()                          # Clear Message Buffer

        if "Status" in data:                      # Status request from Music Pi Received
            print("Bells Pi Status Request Received")
            logger.info("Bells Pi Status Request Received")
            monitor_performance()                 # Update the Processor Stats
            socket.write(Status)                  # Send the Bells Pi status to Music Pi
            socket.pop()                          # Clear Message Buffer
            print ("Status sent to Music Pi")

        elif "Stop Wedding Rings" in data:          # Signal from Music Pi Received
            print("Stop Wedding Rings")
            logger.info("Stop Wedding Rings from Web Controller")
            threading.Thread(target=wedding_rings_stop).start()

        elif "Wedding Changes Start" in data:          # Signal from Music Pi Received
            print("Wedding Changes Start")
            logger.info("Wedding Changes Start from Web Controller")
            threading.Thread(target=wedding_rings_start).start()

        elif "Stop Change Rings" in data:          # Signal from Music Pi Received
            print("Stop Change Rings")
            logger.info("Stop Change Rings from Web Controller")
            threading.Thread(target=change_rings_stop).start()

        elif "Start Change Rings" in data:          # Signal from Music Pi Received
            print("Start Change Rings")
            logger.info("Start Change Rings from Web Controller")
            threading.Thread(target=change_rings_start).start()

        elif "Enable Extended Hours" in data:          # Signal from Music Pi Received
            print("Enable Extended Hours")
            logger.info("Enable Extended Hours from Web Controller")
            threading.Thread(target=extended_hours_trigger).start()

        elif "Disable Extended Hours" in data:          # Signal from Music Pi Received
            print("Disable Extended Hours")
            logger.info("Disable Extended Hours from Web Controller")
            threading.Thread(target=extended_hours_disable).start()

        else:
            print("Error: Unexpected Message Received from Music Pi: ")
            print(data)
            logger.error("Error: Unexpected Message Received from Music Pi: ")
            logger.error(data)

    sched.resume_job(job_id='check_minstermusic_commands')# Restart checking for commands

def chimes_schedule():                      # Schedule functions to be run and start the scheduler
    sched.add_job(hours, 'cron', hour='10')
    sched.add_job(hours, 'cron', hour='11')
    sched.add_job(hours, 'cron', hour='12')
    sched.add_job(hours, 'cron', hour='13')
    sched.add_job(hours, 'cron', hour='14')
    sched.add_job(hours, 'cron', hour='15')
    sched.add_job(hours, 'cron', hour='16')
    sched.add_job(hours, 'cron', hour='17')
    sched.add_job(hours, 'cron', hour='18')
    sched.add_job(hours, 'cron', hour='19')
    sched.add_job(hours, 'cron', hour='20')
    sched.add_job(hours, 'cron', hour='21')
    sched.add_job(hours, 'cron', hour='22')
    sched.add_job(resetnormalopening, 'cron', hour='23')
    sched.add_job(shutdown, 'cron', minute='1')
    sched.add_job(quarters, 'cron', minute='15')
    sched.add_job(quarters, 'cron', minute='30')
    sched.add_job(quarters, 'cron', minute='45')
    sched.add_job(check_minstermusic_commands, 'interval', seconds=1, id='check_minstermusic_commands')

    sched.start()

def init_logging():
    """
    Used as part of the logging initialisation process during startup.
    """

    logger = logging.getLogger('Minster Software') #pylint: disable=redefined-outer-name

    #Remove the console handler.
    logger.handlers = []

    #Set up the timed rotating file handler.
    rotator = loggingtools.CustomLoggingHandler(filename='./logs/Minster-Bells.log',
                                                when="midnight")

    logger.addHandler(rotator)

    #Set up the formatter.
    formatter = logging.Formatter(fmt='%(asctime)s - %(name)s - %(levelname)s: %(message)s',
                                  datefmt='%d/%m/%Y %H:%M:%S')

    rotator.setFormatter(formatter)

    #Default logging level of INFO.
    logger.setLevel(logging.INFO)
    rotator.setLevel(logging.INFO)

    return logger, rotator


################################################ Main Program Begins Here  ###########################################

if __name__ == "__main__":
    #---------- SET UP THE LOGGER ----------
    logger, handler = init_logging()

    from Tools import sockettools as socket_tools

try:
    startup()                               # Record Startup message and initialise sockets

    chimes_schedule()

    # Initialise stats which haven't been set yet.
    Status[11] = "Disabled"
    Status[12] = "Not Playing"

    time.sleep(10)                          # Allow time for the CPU to settle.

    monitor_performance()                   # Initialise the readings.  This will be called on demand after this.
    print (Status)
    print ("")

    set_volume()

    check_silent_hours()

    while 1:                                # Otherwise, idle until Opening Hours is true and an event occurs.
        time.sleep(2**31-1)

#Catch any unexpected errors and log them so we know what happened.
except Exception:
    logger.critical("Unexpected error \n\n"+str(traceback.format_exc())
                    +"\n\nwhile running. Exiting...")

    print("Unexpected error \n\n"+str(traceback.format_exc())+"\n\nwhile running. Exiting...")

    socket.wait_for_handler_to_exit()
    socket.reset()
