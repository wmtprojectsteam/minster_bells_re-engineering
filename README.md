# Minster_Bells_Re-engineering

This project covers the work to re-engineer the hard and software created as part of the original 2016/17 covered at https://gitlab.com/wmtprojectsteam/Minster_Bells_and_Webserver.

The original hardware was very susceptible to solar gain on the walls of the Minster and the audio quality was very poor.

The new hardware has split the functions over two Raspberry Pis, each equipped with an Adafruit 3 W Amplifier HAT and the old switched based control has been replaced by a Web App accessible over the WMT Intranet. 
